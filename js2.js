var MatrMorf = {
	normMatr: function(matr){
		var d = Math.floor(matr.length / 2);
		return MatrSv.genMatr(matr.length, (i,j) => matr[i+d][j+d], true);
	},
	plus: function(monoB, matrS, monoRes, width){
		function x(v){
			return v % width;
		}
		function y(v){
			return Math.floor(v / width);
		}
		function ii(x,y){
			return x + y * width;
		}
		var sx = Math.floor(matrS.length / 2);
		var sy = Math.floor(matrS[0].length / 2);
		for(var i = 0; i < monoB.length; i++){
			if(monoB[i] > 0.5){
				for(var j = 0; j < matrS.length; j++){
					for(var k = 0; k < matrS[0].length; k++){
						if(matrS[j][k] > 0.5){
							monoRes[ii(x(i)+j-sx, y(i)+k-sy)] = 1;
						}
					}
				}
			}
		}
	},
	minus: function(monoB, matrS, monoRes, width, isNorma){
		if(!isNorma){
			matrS = this.normMatr(matrS);
		}
		MatrSv.matr4mono(monoB, monoRes, width, monoB.length / width, matrS);
		MatrSv.mono2threshold(monoRes, monoRes, 1 - Math.pow(0.1, matrS.length));
	},
	invert: function(monoIn, monoOut, max){
		if(max === undefined){
			max = 1;
		}
		for(var i = 0; i < monoIn.length; i++){
			monoOut[i] = max - monoIn[i];
		}
	},
	close: function(monoB, matrS, monoRes, width){
		var monoBuf = new Array(monoB.length);
		this.plus(monoB, matrS, monoBuf, width);
		this.minus(monoBuf, matrS, monoRes, width);
	},
	open: function(monoB, matrS, monoRes, width){
		var monoBuf = new Array(monoB.length);
		this.minus(monoB, matrS, monoBuf, width);
		this.plus(monoBuf, matrS, monoRes, width);
	},
	newMatrSqrt: function(size, isNorm){
		return MatrSv.genMatr(size, (i,j) => 1, isNorm);
	},
	counter: function(el){
		var rgb = getData(); // get RGB
		var mono = MatrSv.newMono(rgb);
		MatrSv.rgb2monoL(rgb.data, mono); // RGB => MONO
		MatrSv.methodOtsu(mono, mono, 0.1); // MONO => binary => MONO
		this.invert(mono, mono); // MONO => invert => MONO (costil)
		MatrSv.mono2rgb(mono, rgb.data, mono.length, (arr, x) => arr[x]); // MONO => RGB
		putData(rgb); // out RGB
		var slf = this;
		var pos = 0;
		var matr = slf.newMatrSqrt(7);
		var monoRes = MatrSv.newMono(rgb);
		function too(){
			var t = new Date().getTime();
			var res = null;
			switch(pos){
				case 0:
					slf.plus(mono, matr, monoRes, rgb.width);
					break;
				case 1:
					slf.minus(mono, matr, monoRes, rgb.width);
					break;
				case 2:
					slf.open(mono, matr, monoRes, rgb.width);
					break;
				case 3:
					slf.close(mono, matr, monoRes, rgb.width);
					break;
				case 4:
					res = MatrSv.joinGr(monoRes, rgb.width, 0.5);
					break;
				case 5:
					return;
			}
			t = new Date().getTime() - t;
			var m = {
				0: 'DILATION',
				1: 'EROSION',
				2: 'OPENING',
				3: 'CLOSING',
				4: 'COUNT'
			};
			el.innerHTML = (m[pos] + ' time ' + t + ' ms');
			if(res == null){
				MatrSv.mono2rgb(monoRes, rgb.data, monoRes.length, (arr, x) => arr[x]);
			} else {
				MatrSv.mono2rgbFull(res[0], rgb.data, monoRes.length, function(mono, pos){
					if(mono[pos] == -1){
						return [0,0,1];
					} else {
						return [mono[pos] / res[1].length, 1, 1];
						// return [0,0,0];
					}
				}, rgb.data);
				alert('Count ' + res[1].length + ' objects');
			}
			putData(rgb);
			pos++;
			setTimeout(too, 1000);
		};
		too();
	}
};

var Splitter = {
	sliRazd: function(monoInn, width, e, deep){
		function simply(){
			var monoRes = new Array(deep * deep);
			var ks = [deep / width, width * deep / monoInn.length];
			var flr = Math.floor;
			for(var i = 0; i < monoInn.length; i++){
				var k = flr((i % width) * ks[0]) + deep * flr(flr(i / width) * ks[1]);
				if(monoRes[k] === undefined){
					monoRes[k] = 0;
				}
				monoRes[k] += deep * deep * monoInn[i] / monoInn.length;
			}
			return monoRes;
		}
		function hardly(monoRes){
			var monoRet = new Array(monoInn.length);
			var ks = [deep / width, width * deep / monoInn.length];
			var flr = Math.floor;
			for(var i = 0; i < monoInn.length; i++){
				var k = flr((i % width) * ks[0]) + deep * flr(flr(i / width) * ks[1]);
				monoRet[i] = monoRes[k];
			}
			return monoRet;
		}
		var monoIn = simply();
		var bufArr = new Array(monoIn.length);
		var map = [];
		function check(pos){
			var n = bufArr[pos];
			if(n === undefined){
				n = map.length;
				map.push([new Set(), monoIn[pos], 1]);
				map[n][0].add(n);
				bufArr[pos] = n;
			}
			function getBCcoors(i, j){
				var xpos = (pos % deep) + i;
				var ypos = Math.floor(pos / deep) + j;
				if(xpos < 0 || xpos >= deep || ypos < 0){
					return -1;
				}
				var npos = xpos + deep * ypos;
				if(npos >= monoIn.length){
					return -1;
				}
				return npos;
			}
			var pos = [pos, getBCcoors(-1, 0), getBCcoors(0, -1)];
			if(pos.indexOf(-1) != -1){
				return;
			}
			var vals = pos.map(el => map[bufArr[el]][1] / map[bufArr[el]][2]);
			var d = Math.abs;
			function adG(posD, posS){
				var mD = map[bufArr[pos[posD]]];
				var mS = map[bufArr[pos[posS]]];
				mD[0].add(bufArr[pos[posS]]);
				mS[0].add(bufArr[pos[posD]]);
				mD[1] += mS[1];
				mS[1] += mD[1];
				mD[2] += mS[2];
				mS[2] += mD[2];
			}
			if(d(vals[0] - vals[1]) > e){
				if(d(vals[0] - vals[2]) > e){
					return;
				} else {
					adG(0, 2);
				}
			} else {
				if(d(vals[0] - vals[2]) > e){
					adG(0, 1);
				} else {
					if(d(vals[1] - vals[2]) > e){
						if(d(vals[0] - vals[2]) < d(vals[0] - vals[1])){
							adG(0, 2);
						} else {
							adG(0, 1);
						}
					} else {
						adG(0, 1);
						adG(1, 2);
					}
				}
			}
		}
		var min = 0, max = 0, minEl = monoIn[0], maxEl = monoIn[0];
		for(var i = 0; i < monoIn.length; i++){
			if(minEl > monoIn[i]){
				min = i;
				minEl = monoIn[i];
			}
			if(maxEl < monoIn[i]){
				maxEl = monoIn[i];
				max = i;
			}
			check(i);
		}
		function cls(pos, realPos){
			if(typeof(map[pos][0]) != "object"){
				return;
			}
			var poses = map[pos][0];
			map[pos][0] = realPos;
			for(var i of poses){
				cls(i, realPos);
			}
		}
		for(var i = 0; i < map.length; i++){
			cls(i, i);
		}
		var counter = new Set();
		for(var i = 0; i < bufArr.length; i++){
			if(bufArr[i] === undefined){
				bufArr[i] = -1;
			} else {
				bufArr[i] = map[bufArr[i]][0];
				counter.add(bufArr[i]);
			}
		}
		return [hardly(bufArr), Array.from(counter.values()), min, max];
	},
	checkSliRazd: function(e){
		var dataIn = getData();
		var monoIn = MatrSv.newMono(dataIn);
		MatrSv.rgb2mono(dataIn.data, monoIn);
		var res = Splitter.sliRazd(monoIn, dataIn.width, e, 100);
		MatrSv.mono2rgbFull(res[0], dataIn.data, monoIn.length, function(mono, pos){
			if(mono[pos] == -1){
				return [0,0,1];
			} else {
				return [mono[pos] / res[1].length, 1, 1];
				// return [0,0,0];
			}
		}, dataIn.data);
		putData(dataIn);
	}
};