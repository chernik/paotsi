function newPic(cb){
	fileLoader.onchange = function(e){
		putFile(cb);
	}
	fileLoader.click();
}

function putFile(cb){
	var file = fileLoader.files[0];
	filePic.src = URL.createObjectURL(file);
	filePic.onload = function(e){
		canvas.width = filePic.width;
		canvas.height = filePic.height;
		canvas.getContext('2d').drawImage(filePic, 0, 0);
		if(cb !== undefined){
			cb();
		}
	}
}

function getData(){
	return canvas.getContext('2d').getImageData(0,0,canvas.width,canvas.height);
}

function putData(data){
	canvas.getContext('2d').putImageData(data,0,0);
}

function newData(data){
	return new ImageData(data.width, data.height);
}

function rgb2hsv(data, pos, hsv){
	// rgb [0;255]
	// h [0;360]
	// s [0;1]
	// v [0;255]
	// -------------------------
	// find max
	var min = data[pos*4+0];
	var max = data[pos*4+0];
	for(var i = 1; i < 3; i++){
		if(min > data[pos*4+i]){
			min = data[pos*4+i];
		}
		if(max < data[pos*4+i]){
			max = data[pos*4+i];
		}
	}
	// match S & V
	if(hsv === undefined){
		hsv = [0,0,0];
	}
	hsv[1] = 1 - min / max;
	hsv[2] = max / 255;
	if(min == max){
		hsv[0] = 0;
		return hsv;
	}
	// other
	var r = data[pos*4+0];
	var g = data[pos*4+1];
	var b = data[pos*4+2];
	if(max == r){
		if(g < b){
			hsv[0] = (g-b)/(max - min)/6 + 3/3;
		} else {
			hsv[0] = (g-b)/(max - min)/6 + 0/3;
		}
	} else if(max == g){
		hsv[0] = (b-r)/(max - min)/6 + 1/3;
	} else if(max == b){
		hsv[0] = (r-g)/(max - min)/6 + 2/3;
	}
	return hsv;
}

function rgb2xyz(data, pos, xyz){
	if(xyz === undefined){
		xyz = [0,0,0];
	}
	var k = 255;
	xyz[0] = data[pos*4+0] / k * 2.768892 + data[pos*4+1] / k * 1.751748 + data[pos*4+2] / k * 1.130160;
	xyz[1] = data[pos*4+0] / k * 1.000000 + data[pos*4+1] / k * 4.590700 + data[pos*4+2] / k * 0.060100;
	xyz[2] = data[pos*4+0] / k * 0.000000 + data[pos*4+1] / k * 0.056508 + data[pos*4+2] / k * 5.594292;
	return xyz;
}

function xyz2lab(xyz, lab){
	function f(x){
		if(x > Math.pow(6/29, 3)){
			return Math.pow(x, 1/3);
		}
		return 1/3*29*29/6/6*x + 4/29;
	}
	var white = [95.04, 100.0, 108.88];
	if(lab === undefined){
		lab = [0,0,0];
	}
	lab[0] = 116 * f(xyz[1]/white[1]) - 16;
	lab[1] = 500 * (f(xyz[0]/white[0]) - f(xyz[1]/white[1]));
	lab[2] = 200 * (f(xyz[1]/white[1]) - f(xyz[2]/white[2]));
	return lab;
}

function rgb2lab(data, pos, xyz, lab){
	if(xyz === undefined){
		return xyz2lab(rgb2xyz(data, pos));
	} else {
		rgb2xyz(data, pos, xyz);
		xyz2lab(xyz, lab);
		return lab;
	}
}

function hsv2rgb(hsv, data, pos){
	var ind = Math.floor(hsv[0] * 6) % 6;
	var V = hsv[2] * 255;
	var Vmin = (1 - hsv[1]) * V;
	var a = (V - Vmin) * (Math.floor(hsv[0] * 360) % 60) / 60;
	var Vinc = Vmin + a;
	var Vdec = V - a;
	switch(ind){
	case 0:
		data[pos*4 + 0] = V;
		data[pos*4 + 1] = Vinc;
		data[pos*4 + 2] = Vmin;
		break;
	case 1:
		data[pos*4 + 0] = Vdec;
		data[pos*4 + 1] = V;
		data[pos*4 + 2] = Vmin;
		break;
	case 2:
		data[pos*4 + 0] = Vmin;
		data[pos*4 + 1] = V;
		data[pos*4 + 2] = Vinc;
		break;
	case 3:
		data[pos*4 + 0] = Vmin;
		data[pos*4 + 1] = Vdec;
		data[pos*4 + 2] = V;
		break;
	case 4:
		data[pos*4 + 0] = Vinc;
		data[pos*4 + 1] = Vmin;
		data[pos*4 + 2] = V;
		break;
	case 5:
		data[pos*4 + 0] = V;
		data[pos*4 + 1] = Vmin;
		data[pos*4 + 2] = Vdec;
		break;
	}
	data[pos*4 + 3] = 255;
}

function outText(text, coor){
	if(coor === undefined){
		txtUp.style.display = "none";
		return;
	}
	txtUp.style.top =  (10+coor[1]) + "px";
	txtUp.style.left = (10+coor[0]) + "px";
	txtUp.innerHTML = text;
	txtUp.style.display = "block";
}

function setOnMove(){
	canvas.onmousemove = function(e){
		var coor = [e.offsetX, e.offsetY];
		coor = coor[0] + canvas.width * coor[1];
		var data = getData();
		var resHSV = rgb2hsv(data.data, coor);
		resHSV[0] = Math.floor(resHSV[0] * 360);
		var resLAB = xyz2lab(rgb2xyz(data.data, coor));
		var tableTG = "<table border=\"1px\">";
		var trTG = "<td style=\"width:166px\">";
		var res = tableTG + "<tr>" + trTG + [resHSV.join("</td>" + trTG), resLAB.join("</td>" + trTG)].join("</td></tr><tr>" + trTG) + "</td></tr></table>";
		outText(res, [e.clientX, e.clientY]);
	}
	canvas.onmouseleave = function(e){
		outText();
	}
}

function changeHSVfull(opt, delta, dataIn, dataOut, pos, hsv){
	rgb2hsv(dataIn, pos, hsv);
	function ram1(v){
		while(v < 0){
			v += 1;
		}
		while(v > 1){
			v -= 1;
		}
		return v;
	}
	function ram2(v){
		if(v < 0){
			v = 0;
		}
		if(v > 1){
			v = 1;
		}
		return v;
	}
	hsv[opt] = ram2(hsv[opt] + delta);
	hsv2rgb(hsv, dataOut, pos);
}

function changeHSV(opt, delta, dataIn, dataOut){
	if(dataIn === undefined){
		var data = getData();
		var t = new Date().getTime();
		changeHSV(opt, delta, data.data, data.data);
		t = new Date().getTime() - t;
		putData(data);
		return t;
	}
	var len = dataIn.length / 4;
	var hsv = [0,0,0];
	for(var i = 0; i < len; i++){
		changeHSVfull(opt, delta, dataIn, dataOut, i, hsv);
	}
	return;
}

function goBeg(cb, fl){
	var dataIn = getData();
	var dataOut = newData(dataIn);
	var k = +begCont.max / 2;
	var lastValue = 0;
	begCont.value = k;
	begContDiv.style.display = "block";
	begContText.innerHTML = "";
	window.onkeydown = function(e){
		if(e.keyCode == 27){
			begContDiv.style.display = "none";
			begCont.onmousemove = null;
			window.onmouseup = null;
			window.onkeydown = null;
		}
	}
	var cbb = function(e){
		var val = (+begCont.value - k) / k;
		if(val == lastValue){
			return;
		}
		val = cb(val, dataIn, dataOut);
		if(val === undefined){
			begContText.innerHTML = "";
		} else {
			begContText.innerHTML = val;
		}
		putData(dataOut);
	}
	if(fl === undefined){
		begCont.onmousemove = cbb;
	} else {
		window.onmouseup = cbb;
	}
}

function changeHSVgui(opt){
	goBeg(function(val, dataIn, dataOut){
		changeHSV(opt, val, dataIn.data, dataOut.data);
		return val;
	});
}

function getGist(f, e, len){
	var map = new Map();
	var t = new Date().getTime();
	var pos, count;
	for(var i = 0; i < len; i++){
		pos = Math.floor(f(i) / e) * e;
		count = (map.get(pos) || 0) + 1;
		map.set(pos, count);
	}
	t = new Date().getTime() - t;
	console.log('Collect hitogram > ' + t);
	var res = Array.from(map);
	res.sort(function(a,b){
		return a[0] - b[0];
	});
	return res;
}

function getGistL(data, e){
	var xyz = new Array(3);
	var lab = new Array(3);
	function f(pos){
		rgb2lab(data, pos, xyz, lab);
		return lab[0];
	}
	return getGist(f, e, data.length / 4);
}

function getGistV(data, e){
	function f(pos){
		var hsv = rgb2hsv(data, pos);
		return hsv[2];
	}
	return getGist(f, e, data.length / 4);
}

function outGraph(res, k){
	graph.width += 0;
	var f1 = res[res.length-1][0];
	var f2 = +graph.width;
	var min = res[0][0];
	var h = +graph.height;
	var ctx = graph.getContext("2d");
	ctx.beginPath();
	for(var i = 0; i < res.length; i++){
		ctx.lineTo(res[i][0] * f2 / f1, h - res[i][1] / k);
	}
	ctx.stroke();
	ctx.fillText(min + ".." + f1, 100, 10);
}

function outGistL(){
	var data = getData().data;
	var t = new Date().getTime();
	var res = getGistL(data, 0.1);
	t = new Date().getTime() - t;
	outGraph(res, 150);
	return t;
}

function reloadPic(){
	putFile();
}

window.onkeyup = function(e){
	if(e.ctrlKey && e.keyCode == 81){ // Ctrl + Q
		reloadPic();
	}
}

// -------------------------------------------------------------------------------------------------------------

var MatrSv = {
	rgb2mono: function(dataRGB, dataMONO){
		var len = dataMONO.length;
		var hsv = [0,0,0];
		for(var i = 0; i < len; i++){
			rgb2hsv(dataRGB, i, hsv);
			dataMONO[i] = hsv[2];
		}
	},
	rgb2monoL: function(dataRGB, dataMONO){
		var len = dataMONO.length;
		var xyz = new Array(3);
		var lab = new Array(3);
		for(var i = 0; i < len; i++){
			rgb2lab(dataRGB, i, xyz, lab);
			dataMONO[i] = lab[0];
		}
	},
	matr4mono1: function(dataMONO, mtx, wPic, x, y){
		var res = 0;
		var lX = mtx.length;
		var lY = mtx[0].length;
		var dX = (lX-1)/2;
		var dY = (lY-1)/2;
		x -= dX;
		y -= dY;
		var ind = 0;
		for(var i = 0; i < lX; i++){
			for(var j = 0; j < lY; j++){
				ind = (x + i) + (y + j) * wPic;
				ind = dataMONO[ind];
				// ind = 0;
				ind *= mtx[i][j];
				res += ind;
			}
		}
		return res;
	},
	matr4mono: function(dataMONO, resMONO, sX, sY, mtx){
		for(var i = 0; i < sX; i++){
			for(var j = 0; j < sY; j++){
				resMONO[i + j * sX] = this.matr4mono1(dataMONO, mtx, sX, i, j);
			}
		}
	},
	mono2rgb: function(dataMONO, dataRGB, len, f){
		var hsv = [0,0,0];
		for(var i = 0; i < len; i++){
			hsv[2] = f(dataMONO, i);
			hsv2rgb(hsv, dataRGB, i);
		}
	},
	mono2rgbMore: function(dataMONO, dataRGB, len, f, dataBase){
		var hsv = [0,0,0];
		for(var i = 0; i < len; i++){
			rgb2hsv(dataBase, i, hsv);
			hsv[2] = f(dataMONO, i);
			hsv2rgb(hsv, dataRGB, i);
		}
	},
	mono2rgbFull: function(dataMONO, dataRGB, len, f, dataBase){
		var hsv = [0,0,0];
		for(var i = 0; i < len; i++){
			hsv = f(dataMONO, i);
			hsv2rgb(hsv, dataRGB, i);
		}
	},
	newMono: function(dataRGB){
		return new Float64Array(dataRGB.width * dataRGB.height);
		// return new Array(dataRGB.width * dataRGB.height);
	},
	getTime: function(){
		return new Date().getTime();
	},
	sTime: function(){
		this.t = this.getTime();
	},
	fTime: function(txt){
		this.t = this.getTime() - this.t;
		console.log(txt + " > " + this.t);
		this.sTime();
	},
	filterSobol: function(dataIn, dataOut){
		var monoIn = this.newMono(dataIn);
		var monoOut = [
			this.newMono(dataIn),
			this.newMono(dataIn)
		];
		var mtx = [[
			[-1, 0, +1],
			[-2, 0, +2],
			[-1, 0, +1]
		], [
			[-1, -2, -1],
			[ 0,  0,  0],
			[+1, +2, +1]
		]];
		this.sTime();
		this.rgb2mono(dataIn.data, monoIn);
		this.fTime("rgb2mono");
		this.sTime()
		this.matr4mono(monoIn, monoOut[0], dataIn.width, dataIn.height, mtx[0]);
		this.fTime("match V");
		this.matr4mono(monoIn, monoOut[1], dataIn.width, dataIn.height, mtx[1]);
		this.fTime("match H");
		this.mono2rgbMore(monoOut, dataOut.data, monoOut[0].length, function(mono, pos){
			return Math.sqrt(mono[0][pos] * mono[0][pos] + mono[1][pos] * mono[1][pos]);
		}, dataIn.data);
	},
	genMatr: function(size, f, isNorm){
		var d = (size-1)/2;
		var mtx = [];
		var sum = 0;
		for(var i = -d; i < size-d; i++){
			mtx[i+d] = [];
			for(var j = -d; j < size-d; j++){
				mtx[i+d][j+d] = f(i, j);
				sum += mtx[i+d][j+d];
			}
		}
		if(isNorm){
			for(var i = 0; i < size; i++){
				for(var j = 0; j < size; j++){
					mtx[i][j] /= sum;
				}
			}
		}
		return mtx;
	},
	genMatrGauss: function(size, qDisp){
		return this.genMatr(size, function(i, j){
			return Math.exp( -(i*i+j*j)/2/qDisp ) / (2*Math.PI*qDisp);
		}, true);
	},
	genMatrGabor: function(size, gamma, fi, lambda, teta){
		return this.genMatr(size, function(i, j){
			teta = teta * Math.PI / 180;
			i =  i * Math.cos(teta) + j * Math.sin(teta);
			j = -i * Math.sin(teta) + j * Math.cos(teta);
			var sigma = lambda * 0.56;
			var F = 1 / lambda;
			return Math.exp(-(i*i + gamma*gamma * j*j) / (2 * sigma*sigma))
				* Math.cos(2 * Math.PI * F * i + fi);
		}, true);
	},
	filterGauss: function(dataIn, dataOut, size, qDisp){
		var monoIn = this.newMono(dataIn);
		var monoOut = this.newMono(dataIn);
		this.rgb2mono(dataIn.data, monoIn);
		var mtx = this.genMatrGauss(size, qDisp);
		this.sTime();
		this.matr4mono(monoIn, monoOut, dataIn.width, dataIn.height, mtx);
		this.fTime("gauss");
		this.mono2rgbMore(monoOut, dataOut.data, monoOut.length, function(mono, pos){
			return mono[pos];
		}, dataIn.data);
	},
	joinGr: function(monoIn, width, e){
		var bufArr = new Array(monoIn.length);
		var map = [];
		function check(pos){
			if(monoIn[pos] < e){
				return;
			}
			var n = bufArr[pos];
			if(n === undefined){
				n = map.length;
				map.push(new Set());
				map[n].add(n);
			}
			for(var i = -1; i <= 1; i++){
				for(var j = -1; j <= 1; j++){
					var xpos = (pos % width) + i;
					var ypos = Math.floor(pos / width) + j;
					if(xpos < 0 || xpos >= width || ypos < 0){
						continue;
					}
					var npos = xpos + width * ypos;
					if(npos >= monoIn.length){
						continue;
					}
					if(monoIn[npos] < e){
						continue;
					}
					if(bufArr[npos] === undefined){
						bufArr[npos] = n;
					} else {
						map[bufArr[npos]].add(n);
					}
				}
			}
		}
		var min = 0, max = 0, minEl = monoIn[0], maxEl = monoIn[0];
		for(var i = 0; i < monoIn.length; i++){
			if(minEl > monoIn[i]){
				min = i;
				minEl = monoIn[i];
			}
			if(maxEl < monoIn[i]){
				maxEl = monoIn[i];
				max = i;
			}
			check(i);
		}
		function cls(pos, realPos){
			if(typeof(map[pos]) != "object"){
				return;
			}
			var poses = map[pos];
			map[pos] = realPos;
			for(var i of poses){
				cls(i, realPos);
			}
		}
		for(var i = 0; i < map.length; i++){
			cls(i, i);
		}
		var counter = new Set();
		for(var i = 0; i < bufArr.length; i++){
			if(bufArr[i] === undefined){
				bufArr[i] = -1;
			} else {
				bufArr[i] = map[bufArr[i]];
				counter.add(bufArr[i]);
			}
		}
		return [bufArr, Array.from(counter.values()), min, max];
	},
	algKanni: function(dataIn, dataOut, val){
		var mono = MatrSv.newMono(dataIn);
		MatrSv.rgb2mono(dataIn.data, mono);
		var res = MatrSv.joinGr(mono, dataIn.width, val);
		MatrSv.mono2rgbFull(res[0], dataOut.data, mono.length, function(mono, pos){
			if(mono[pos] == -1){
				return [0,0,1];
			} else {
				return [mono[pos] / res[1].length, 1, 1];
				// return [0,0,0];
			}
		}, dataIn.data);
		return {
			count: res[1].length,
			minMono: res[2],
			maxMono: res[3],
			res: res
		};
	},
	filterGabor: function(dataIn, dataOut, teta){
		var monoIn = this.newMono(dataIn);
		var monoOut = this.newMono(dataOut);
		this.rgb2mono(dataIn.data, monoIn);
		// size, gamma, fi, lambda, teta
		var mtx = this.genMatrGabor(3, 0.1, 0, 2, teta);
		this.matr4mono(monoIn, monoOut, dataIn.width, dataIn.height, mtx);
		this.mono2rgbMore(monoOut, dataOut.data, monoOut.length, function(mono, pos){
			return mono[pos];
		}, dataIn.data);
	},
	getHist: function(mono, e){
		return getGist(i => mono[i], e, mono.length);
	},
	getOtsu: function(hist){
		function sum(a,b,f){
			var s = 0;
			for(var i = a; i <= b; i++){
				s += f(i);
			}
			return s;
		}
		var n = sum(0,hist.length-1, // count of pixels
			x => hist[x][1]
		);
		function w0(k){ // probapility <= k
			return sum(0,k,x => hist[x][1] / n);
		}
		function w1(k){ // probability > k
			return 1 - w0(k);
		}
		function nu0(k){ // from pptx
			var w = w0(k);
			return sum(0,k,
				x => hist[x][0] * hist[x][1] / n / w
			);
		}
		function nu1(k){ // from pptx
			var w = w1(k);
			return sum(k+1,hist.length-1,
				x => hist[x][0] * hist[x][1] / n / w
			);
		}
		function sigK(k){ // from pptx
			return w0(k) * w1(k) * Math.pow(nu1(k) - nu0(k), 2);
		}
		var max = 1, maxV = 0;
		for(var i = 0; i <= hist.length-1; i++){ // find max
			var level = sigK(i);
			if(level > maxV){
				maxV = level;
				max = i;
			}
		}
		return max;
	},
	mono2threshold: function(monoIn, monoOut, th){
		for(var i = 0; i < monoIn.length; i++){
			monoOut[i] = +(monoIn[i] >= th);
		}
	},
	methodOtsu: function(monoIn, monoOut, e){
		var hist = this.getHist(monoIn, e);
		if(hist.length < 2){
			alert('One value in histogram!');
			return;
		}
		var thi = this.getOtsu(hist);
		var th = hist[thi][0];
		this.mono2threshold(monoIn, monoOut, th);
	}

};

function filterSobol(){
	var data = getData();
	MatrSv.filterSobol(data, data);
	putData(data);
}

function filterGaussSize(){
	var lastVal = -1;
	goBeg(function(val, dataIn, dataOut){
		val = (val + 1) / 2;
		val = Math.floor(val * 15);
		if((val % 2) == 0){
			val += 1;
		}
		if(val == lastVal){
			return;
		}
		lastVal = val;
		alert("Match for " + val + ", wait please");
		MatrSv.filterGauss(dataIn, dataOut, val, 1);
		alert("Sucs!");
		return val;
	}, true);
}

function filterGaussDisp(){
	goBeg(function(val, dataIn, dataOut){
		val = (val + 1) / 2 * 4;
		alert("Match for " + val + ", wait please");
		MatrSv.filterGauss(dataIn, dataOut, 5, val);
		alert("Sucs!");
		return val;
	}, true);
}

function algKanni(fl){
	if(fl){
		var data = getData();
		alert('Wait for premathing...');
		MatrSv.filterGauss(data, data, 5, 2.3);
		MatrSv.filterSobol(data, data);
		alert('Choose rank');
		putData(data);
	}
	goBeg(function(val, dataIn, dataOut){
		val = (val + 1) / 2;
		var res = MatrSv.algKanni(dataIn, dataOut, val);
		return val;
	}, true);
}

function filterGabor(){
	goBeg(function(val, dataIn, dataOut){
		val = (val + 1) / 2 * 180;
		MatrSv.filterGabor(dataIn, dataOut, val);
		return val;
	}, true);
}

function methodOtsu(e){
	if(e === undefined){
		e = 0.1;
	}
	var data = getData(); // Pic data
	var monoIn = MatrSv.newMono(data);
	var t = new Date().getTime();
	MatrSv.rgb2monoL(data.data, monoIn);
	t = new Date().getTime() - t;
	console.log('Collect L > ' + t);
	t = new Date().getTime();
	MatrSv.methodOtsu(monoIn, monoIn, e);
	t = new Date().getTime() - t;
	console.log('Otsu > ' + t);
	MatrSv.mono2rgb(monoIn, data.data, monoIn.length, (arr, x) => arr[x]);
	putData(data);
}